(ns com.arrival.task-registry.domain.task.handler-test
  ""
  {:author "Roman Dronov"}
  (:require [clojure.test :refer [is deftest testing use-fixtures]]
            [clojure.instant :as i]
            [clojure.core.async :as a :refer [<!! close!]]
            [com.arrival.task-registry.domain.task.core :as core]
            [com.arrival.task-registry.db.test-helpers :refer [*conn* with-db]]))

(use-fixtures :each with-db)

(deftest task-handler-retrieve-on-empty
  (testing "Retrieve on empty connection should not produce any tasks"
    (let [tasks-chan (core/fetch *conn*)
          tasks (<!! tasks-chan)]
      (close! tasks-chan)
      (is (= [] tasks)))))

(deftest task-handler-insert-one
  (testing "Retrieve after single insert should produce exactley one task"
    (let [created-task (<!! (core/save *conn* {:title "a"
                                               :description "clojure"
                                               :due-date "2021-04-20"
                                               :executor "You"
                                               :applicant "me"}))
          tasks-chan (core/fetch *conn*)
          tasks (<!! tasks-chan)]
      (close! tasks-chan)
      (is (= [#:task{:id (:task/id created-task)
                     :title "a"
                     :description "clojure"
                     :due-date (i/read-instant-date "2021-04-20")
                     :executor "You"
                     :applicant "me"}] tasks)))))


(deftest task-handler-save-several-entities
  (testing "Retrieve after each insertion should produce correct count of tasks"
    (let [created-task (<!! (core/save *conn* {:title "a"
                                               :description "clojure"
                                               :due-date "2021-04-20"
                                               :executor "You"
                                               :applicant "me"}))
          tasks-chan (core/fetch *conn*)
          tasks (<!! tasks-chan)]
      (close! tasks-chan)
      (is (= [#:task{:id (:task/id created-task)
                     :title "a"
                     :description "clojure"
                     :due-date (i/read-instant-date "2021-04-20")
                     :executor "You"
                     :applicant "me"}] tasks))
      (let [created-task-2 (<!! (core/save *conn* {:title "b"
                                                   :description "The Beatles"
                                                   :due-date "2021-04-21"
                                                   :executor "John George Paul Ringo"
                                                   :applicant "Anybody"}))
            tasks-chan (core/fetch *conn*)
            tasks (<!! tasks-chan)]
        (close! tasks-chan)
        (is (some #(= % #:task{:id (:task/id created-task)
                               :title "a"
                               :description "clojure"
                               :due-date (i/read-instant-date "2021-04-20")
                               :executor "You"
                               :applicant "me"}) tasks))
        (is (some #(= % #:task{:id (:task/id created-task-2)
                               :title "b"
                               :description "The Beatles"
                               :due-date (i/read-instant-date "2021-04-21")
                               :executor "John George Paul Ringo"
                               :applicant "Anybody"}) tasks))))))
