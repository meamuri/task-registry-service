(ns com.arrival.task-registry.db.test-helpers
  "Helpers for testing related to datomic operations stuff"
  {:author "Roman Dronov"}
  (:require [com.arrival.task-registry.db :as db]
            [datomic.api :as d]))

(def ^:dynamic *conn* nil)

(defn with-db
  [f]
  (let [uri (str "datomic:mem://" (d/squuid))]
    (d/create-database uri)
    (let [conn (d/connect uri)]
      @(d/transact conn db/task-registry-schema)
      (binding [*conn* conn]
        (f))
      (.release conn)
      (d/delete-database uri))))
