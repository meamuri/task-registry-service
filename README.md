# Task Registry Service

## Usage

```
./bin/build
```

then:
```
java -jar target/task-registry.jar
```

## check rest api

```
curl --request POST \
  --url http://localhost:8080/api/tasks \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "The Beatles",
	"description": "Listen Deep Prudence",
	"due-date": "2021-04-15",
	"applicant": "John Paul George Ringo",
	"executor": "Me"
}'
```

```
curl --request GET \
  --url http://localhost:8080/api/tasks
```

## Deprecated! Refactoring is required
 ~~You can provide default task list with `$SEED_DATA_PATH` environment variable~~
```
SEED_DATA_PATH=./tasks.edn java -jar target/task-registry.jar
```

~~where `./tasks.edn` has format:~~

```end
{:tasks [{:title "Not empty title"
          :description "Not empty description"
          :due-date "2020-04-25" ;; yyyy-MM-dd format
          :applicant "Not empty"
          :executor "Not empty"}
          
          ...]}
```
