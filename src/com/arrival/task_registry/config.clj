(ns com.arrival.task-registry.config
  (:require [aero.core :refer [read-config]]
            [clojure.java.io]))

(def ^:private config (read-config (clojure.java.io/resource "config.edn")))

(defn ^:private prop [& props]
  (get-in config props))

(defn server-port
  []
  (prop :server :port))

(defn db-connection
  []
  (prop :db :connection))
