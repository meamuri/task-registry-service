(ns com.arrival.task-registry.system
  (:require [com.stuartsierra.component :as component]
            [com.arrival.task-registry.db :as db]
            [com.arrival.task-registry.server :as server]))

(defn system
  [{:keys [db-uri db-schema server-port] :or {server-port 8080}}]
  (component/system-map
   :db (db/create-database db-uri db-schema)
   :server (component/using
            (server/create-server server-port)
            {:database :db})))
