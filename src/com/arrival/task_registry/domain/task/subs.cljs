(ns com.arrival.task-registry.domain.task.subs
  "Subscriptions of task domain"
  {:author "Roman Dronov"}
  (:require [re-frame.core :as re-frame]
            [com.arrival.task-registry.domain.task.events]))

(re-frame/reg-sub
 :tasks
 (fn [db _v]
   (:tasks db)))

(re-frame/reg-sub
 :add-task-form
 (fn [db _v]
   (:add-task-form db)))

(re-frame/reg-sub
 :issues
 (fn [db _v]
   (:issues db)))

(re-frame/reg-sub
 :notifications
 (fn [db _v]
   (:notifications db)))

(re-frame/reg-sub
 :active-page
 (fn [db _v]
   (:active-page db)))

(re-frame/reg-sub
 :current-task
 (fn [db _v]
   (let [id (:current-task db)]
     (->> db
          :tasks
          (filter #(= (:task/id %) id))
          first))))
