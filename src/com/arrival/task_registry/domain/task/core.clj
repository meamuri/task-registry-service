(ns com.arrival.task-registry.domain.task.core
  (:require [com.arrival.task-registry.domain.task.repository :as repo]
            [clojure.core.async :refer [chan go >!]]
            [clojure.instant :as i]))

(defn save
  [conn {:keys [title description due-date executor applicant]}]
  (let [ch (chan)
        task (-> #:task{:title title
                        :description description
                        :due-date due-date
                        :executor executor
                        :applicant applicant}
                 (update :task/due-date i/read-instant-date))]
    (go (->> task
             (repo/create! conn)
             (>! ch)))
    ch))

(defn ^:private tuple-to-task [[id titile description due-date executor applicant]]
  #:task{:id id
         :title titile
         :description description
         :due-date due-date
         :executor executor
         :applicant applicant})

(defn ^:private format-datomic-query-result
  [query-result]
  (->> query-result
       vec
       (map tuple-to-task)))

(defn fetch
  [conn]
  (let [ch (chan)]
    (go (->> conn
             repo/fetch!
             format-datomic-query-result
             (>! ch)))
    ch))
