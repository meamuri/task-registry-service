(ns com.arrival.task-registry.domain.task.view
  ""
  (:require [re-frame.core :as re-frame]
            [com.arrival.task-registry.domain.task.events]
            [com.arrival.task-registry.domain.task.view.control-panel :refer [tasks-control-panel]]
            [com.arrival.task-registry.domain.task.view.tasks-view :refer [tasks-view]]
            [com.arrival.task-registry.domain.task.view.task :refer [task-card]]))

(defn ^:private show-active-page
  [page]
  (case page
    :home [:div.row
           [:div.column [tasks-control-panel]]
           [:div.column [tasks-view]]]
    :task [task-card]
    [:div])) ;; TODO: Trailing div is dirty huck. Prevent routing empty handler and fix it

(defn component
  []
  (let [active-page @(re-frame/subscribe [:active-page])]
    (show-active-page active-page)))
