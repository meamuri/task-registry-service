(ns com.arrival.task-registry.domain.task.handler
  (:require [com.arrival.task-registry.domain.task.schema :as schema]
            [com.arrival.task-registry.domain.task.core :as core]
            [clojure.core.async :refer [chan go >! <! close!]]
            [cheshire.core :refer [generate-string]]
            [clojure.tools.logging :as log]))

(defn task-list-handler
  "List of the whole tasks"
  [conn]
  (fn [_request respond _raise]
    (let [ch (chan)
          tasks-chan (core/fetch conn)]
      (respond {:status 200
                :headers {"Content-Type" "application/json"}
                :body ch})
      (go (let [tasks (<! tasks-chan)
                formatted (generate-string tasks {:date-format "yyyy-MM-dd"})]
            (>! ch formatted)
            (close! ch)
            (close! tasks-chan))))))

(defn add-task-handler
  "Save additional task to the storage"
  [conn]
  (fn [request respond _raise]
    (let [task (:body request)]
      (if (not (schema/valid-task? task))
        (do
          (log/warn (str task) "malformed, request declined")
          (respond {:status 400
                    :headers {"Content-Type" "application/json"}
                    :body {:message "Malformed"}}))
        (let [ch (chan)
              response-chan (core/save conn task)]
          (log/info "Trying to save new task" (str task))
          (respond {:status 201
                    :headers {"Content-Type" "application/json"}
                    :body ch})
          (go
            (let [response (<! response-chan)
                  formatted-response (generate-string response {:date-format "yyyy-MM-dd"})]
              (>! ch formatted-response)
              (close! ch)
              (close! response-chan))))))))
