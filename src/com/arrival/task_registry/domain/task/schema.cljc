(ns com.arrival.task-registry.domain.task.schema
  "Task incoming messages validation"
  (:require [malli.core :as m]))

(defn message [term]
  (str (name term) " must not be empty"))

(def error-messages
  (into {:due-date "Due date must be in a format yyyy-MM-dd (e.g. 2021-12-14)"}
        (map (fn [e] {e (message e)}) [:title :description :applicant :executor])))

(def ^:private Task
  [:map
   [:title
    [:string {:min 1 :max 128}]]
   [:description
    [:string {:min 1 :max 512}]]
   [:due-date
    [:re #"^20[0-4][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$"]]
   [:executor
    [:string {:min 1 :max 64}]]
   [:applicant
    [:string {:min 1 :max 64}]]])

(defn valid-tasks-list?
  [task-list]
  (m/validate [:sequential Task] task-list))

(defn valid-task?
  [task]
  (m/validate Task task))

(defn blame-task
  [task]
  (m/explain Task task))

(comment
  (valid-task? {:title "Dogs"
                :description "Wash my dog"
                :due-date "2023-02-01"
                :applicant "Gorge"
                :executor "Carl"}))
