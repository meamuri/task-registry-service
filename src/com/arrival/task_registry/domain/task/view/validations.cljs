(ns com.arrival.task-registry.domain.task.view.validations
  (:require [clojure.string :refer [capitalize]]
            [re-frame.core :refer [dispatch]]
            [com.arrival.task-registry.domain.task.schema :as schema]))

(defn task-validation-group
  [issues]
  [:<>
   [:h5 "Validation"]
   [:div
    (let [fields (map (fn [e] (first (:in e))) issues)
          known-fields (filter (constantly true) fields)
          messages (map #(% schema/error-messages) known-fields)]
      (for [message messages]
        ^{:key message} [:p (capitalize message)]))
    [:button {:on-click #(dispatch [:close-validations])} "Got it"]]])
