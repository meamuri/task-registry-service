(ns com.arrival.task-registry.domain.task.view.task
  (:require [re-frame.core :as re-frame]
            [com.arrival.task-registry.ui.router :as router]))

(defn task-card
  []
  (let [{:task/keys [title description due-date executor applicant]} @(re-frame/subscribe [:current-task])]
    (if (not title)
      [:h4 "Sorry, task not found"]
      [:<>
       [:h6 [:a {:href (router/url-for :home)} "Back"]]
       [:table
        [:tbody
         [:tr
          [:td "Title"]
          [:td title]]
         [:tr
          [:td "Description"]
          [:td description]]
         [:tr
          [:td "Due-Date"]
          [:td due-date]]
         [:tr
          [:td "Applicant"]
          [:td applicant]]
         [:tr
          [:td "Executor"]
          [:td executor]]]]])))
