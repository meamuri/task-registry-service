(ns com.arrival.task-registry.domain.task.view.notifications
  (:require [re-frame.core :refer [subscribe dispatch]]
            [reagent.core :as r]
            [clojure.string :refer [capitalize join]]))

(defn get-color-scheme
  [notification]
  (if (= (:type notification) :error)
    "error-notification-color"
    "primary-notification-color"))

(def trash-icon-csg
  [:svg {:xmlns "http://www.w3.org/2000/svg"
         :width "16"
         :height "16"
         :fill "currentColor"
         :viewBox "0 0 16 16"}
   [:path
    {:d "M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"}]
   [:path
    {:fill-rule "evenodd"
     :d "M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"}]])

(defn ^:private notification-view
  [_notification]
  (let [additional-classes (r/atom [])]
      (fn [notification]
        (let [color (get-color-scheme notification)
              classes @additional-classes]
          [:div.notification-card
           [:div.notification-panel
            [:div.notification-head {:class color} (-> (:type notification)
                                                       name
                                                       capitalize)]
            [:div.notification-body (:details notification)]]
           [:div.notification-discard
            {:on-click #(dispatch [:remove-notification notification])
             :on-mouse-over #(reset! additional-classes ["background-hidden"])
             :on-mouse-out #(reset! additional-classes [])
             :class (join " " classes)}
            trash-icon-csg]]))))

(defn tasks-notifications
  []
  [:<>
   [:h5 "Notifications"]
   (let [notifications @(subscribe [:notifications])]
     (if (empty? notifications)
       [:p "Nothing to show yet"]
       (for [notification notifications]
         ^{:key (:time notification)} [notification-view notification])))])
