(ns com.arrival.task-registry.domain.task.view.tasks-view
  (:require [re-frame.core :refer [subscribe]]
            [com.arrival.task-registry.ui.router :as router]))

(defn task-item
  [task]
  (let [href (router/url-for :task :id (:task/id task))]
    [:<>
     [:td (:task/title task)]
     [:td (:task/due-date task)]
     [:td [:a {:href href} "Details"]]]))

(defn tasks-view
  []
  (let [tasks @(subscribe [:tasks])]
    [:div#tasks-view
     [:h5 "Tasks"]
     [:table#tasks
      [:thead
       [:tr
        [:th "title"]
        [:th "due-date"]
        [:th]]]
      [:tbody
       (for [task tasks]
         ^{:key (:task/id task)} [:tr [task-item task]])]]]))
