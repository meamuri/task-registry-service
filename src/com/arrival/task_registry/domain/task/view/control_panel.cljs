(ns com.arrival.task-registry.domain.task.view.control-panel
  (:require [re-frame.core :refer [dispatch subscribe]]
            [com.arrival.task-registry.domain.task.view.notifications :refer [tasks-notifications]]
            [com.arrival.task-registry.domain.task.view.validations :refer [task-validation-group]]
            [com.arrival.task-registry.domain.task.schema :as schema]
            [clojure.string :as s]))

(defn on-submit-handler
  [event task]
  (.preventDefault event)
  (let [issues (schema/blame-task task)]
    (if (seq issues)
      (dispatch [:validation-not-passed (:errors issues)])
      (dispatch [:add-task-request task]))))

(defn render-form-group
  [form-state {:keys [group-name additional-props input-type] :or {additional-props {}
                                                                   input-type :input}}]
  (let [default-input-props {:input {:type "text"}
                             :textarea {:rows 4}}]
    [:div.form-row
     [:label (-> group-name name s/capitalize)]
     [input-type (merge (input-type default-input-props)
                        {:value (group-name form-state)
                         :on-change #(dispatch [:update-add-task-form-field group-name (-> % .-target .-value)])}
                        additional-props)]]))

(defn task-add-form
  []
  (let [form-state @(subscribe [:add-task-form])]
    [:form {:on-submit #(on-submit-handler % form-state)}
     [render-form-group form-state {:group-name :title}]
     [render-form-group form-state {:group-name :description :input-type :textarea}]
     [render-form-group form-state {:group-name :due-date :additional-props {:placeholder "yyyy-MM-dd"}}]
     [render-form-group form-state {:group-name :executor}]
     [render-form-group form-state {:group-name :applicant}]
     [:div.form-footer
      [:button#add-task-button {:type "submit"} "Register task"]]]))

(defn tasks-control-panel
  []
  (let [issues @(subscribe [:issues])]
    [:div#control-panel
     [:h5 "Control"]
     [task-add-form]
     (when (seq issues)
       [task-validation-group issues])
     [tasks-notifications]]))
