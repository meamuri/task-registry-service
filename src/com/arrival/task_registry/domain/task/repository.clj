(ns com.arrival.task-registry.domain.task.repository
  (:require [datomic.api :as d]))

(defn create!
  ""
  [conn task]
  (let [task-id (d/squuid)
        tx-data (merge {:task/id task-id} task)]
    (d/transact conn [tx-data])
    tx-data))

(defn fetch!
  ""
  [conn]
  (d/q '[:find ?id ?title ?description ?due-date ?executor ?applicant
         :where
         [?e :task/id ?id]
         [?e :task/title ?title]
         [?e :task/description ?description]
         [?e :task/due-date ?due-date]
         [?e :task/executor ?executor]
         [?e :task/applicant ?applicant]]
       (d/db conn)))
