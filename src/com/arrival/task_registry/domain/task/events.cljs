(ns com.arrival.task-registry.domain.task.events
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]))

(re-frame/reg-event-db
 :set-active-page
 (fn [db [_ page id]]
   (-> db
       (assoc :active-page page)
       (assoc :current-task id))))

(re-frame/reg-event-fx
 :load-tasks
 (fn [_world [_ _]]
   {:http-xhrio
    {:method          :get
     :uri             "/api/tasks"
     :timeout         5000
     :response-format (ajax/json-response-format {:keywords? true})
     :on-success      [:success-tasks-load]
     :on-failure      [:failure-tasks-load]}}))

(re-frame/reg-event-fx
 :add-task-request
 (fn [_world [_ val]]
   {:http-xhrio
    {:method          :post
     :uri             "/api/tasks"
     :params          val
     :timeout         5000
     :format          (ajax/json-request-format)
     :response-format (ajax/json-response-format {:keywords? true})
     :on-success      [:success-task-add]
     :on-failure      [:failure-task-add]}}))

(re-frame/reg-event-db
 :add-task
 (fn [db [_ new-task]]
   (let [tasks (:tasks db)
         tasks* (conj tasks new-task)]
     (assoc db :tasks tasks*))))

(re-frame/reg-event-db
 :failure-task-add
 (fn [db [_ response]]
   (->> response
       (str "Error during server interaction")
       js/console.log)
   (let [notification {:type :error
                       :details "Add task request error"
                       :time (.toISOString (js/Date.))}
         notifications (:notifications db)
         notifications* (conj notifications notification)]
     (assoc db :notifications notifications*))))

(re-frame/reg-event-db
 :success-tasks-load
 (fn [db [_ tasks]]
   (assoc db :tasks tasks)))

(re-frame/reg-event-db
 :update-add-task-form-field
 (fn [db [_ field value]]
   (assoc-in db [:add-task-form field] value)))

(re-frame/reg-event-db
 :clean-add-task-form
 (fn [db _]
   (assoc db :add-task-form {:title ""
                             :description ""
                             :due-date ""
                             :executor ""
                             :applicant ""})))

(re-frame/reg-event-fx
 :success-task-add
 (fn [{:keys [db]} [_ task]]
   (let [{:keys [tasks notifications]} db
         tasks* (conj tasks task)
         notifications* (conj notifications {:type :success
                                             :details "New task was added"
                                             :time (.toISOString (js/Date.))})
         new-db (-> db
                    (assoc :tasks tasks*)
                    (assoc :notifications notifications*))]
     {:db new-db
      :dispatch-n [[:clean-add-task-form]
                   [:close-validations]]})))

(re-frame/reg-event-db
 :remove-notification
 (fn [db [_ notification]]
   (js/console.log "Attempt to remove notification" (str notification))
   (let [notifications (:notifications db)
         notifications* (filter #(not= notification %) notifications)]
     (assoc db :notifications notifications*))))

(re-frame/reg-event-db
 :validation-not-passed
 (fn [db [_ issues]]
   (assoc db :issues issues)))

(re-frame/reg-event-db
 :close-validations
 (fn [db _]
   (assoc db :issues ())))
