(ns com.arrival.task-registry.ui.router
  (:require [bidi.bidi :as bidi]
            [pushy.core :as pushy]
            [re-frame.core :as re-frame]))

(def routes ["/" {"testapp"      :home
                  ["tasks/" :id] :task}])

(defn ^:private parse-url
  [url]
  (bidi/match-route routes url))

(defn ^:private dispatch-route
  [matched-route]
  (let [page-name (:handler matched-route)
        params (get-in matched-route [:route-params :id])]
    (re-frame/dispatch [:set-active-page page-name params])))

(defn app-routes []
  (pushy/start! (pushy/pushy dispatch-route parse-url)))

(def url-for (partial bidi/path-for routes))
