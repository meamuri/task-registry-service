(ns com.arrival.task-registry.ui.app
  (:require [reagent.dom]
            [re-frame.core :as re-frame]
            [day8.re-frame.http-fx]
            [com.arrival.task-registry.ui.router :as r]
            [com.arrival.task-registry.ui.db]
            [com.arrival.task-registry.domain.task.subs]
            [com.arrival.task-registry.domain.task.view :as task-view]))

(defn root
  []
  [:<>
   [:h3 "Tasks registry"]
   [task-view/component]])

(defn render
  []
  (reagent.dom/render [root] (.getElementById js/document "root")))

(defn ^:private mount
  []
  (re-frame/clear-subscription-cache!)
  (r/app-routes)
  (re-frame/dispatch-sync [:initialize])
  (re-frame/dispatch [:load-tasks])
  (render))

(defn ^:export init
  []
  (mount))

(defn ^:export refresh
  "Refresh hook for devtools"
  []
  (mount))
