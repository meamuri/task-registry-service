(ns com.arrival.task-registry.ui.db
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-event-db
 :initialize
 (fn [db _]
   (merge db {:tasks []
              :issues '()
              :notifications '()
              :add-task-form {:title ""
                              :description ""
                              :due-date ""
                              :executor ""
                              :applicant ""}})))
