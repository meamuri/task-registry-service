(ns com.arrival.task-registry.server
  (:require [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [com.arrival.task-registry.router :as router]
            [clojure.java.io :as io]
            [clojure.core.async :as a :refer [<! go]]
            [ring.core.protocols :refer [StreamableResponseBody]]
            [com.stuartsierra.component :as component]))

(extend-type clojure.core.async.impl.channels.ManyToManyChannel
  StreamableResponseBody
  (write-body-to-stream [channel response output-stream]
    (go (with-open [writer (io/writer output-stream)]
          (a/loop []
            (when-let [msg (<! channel)]
              (doto writer (.write msg) (.flush))
              (recur)))))))

(defn app
  [conn]
  (-> conn
      router/get-app
      (wrap-json-body {:keywords? true})
      wrap-json-response
      (wrap-resource "public")
      (wrap-content-type
       {:mime-types
        {"css" "text/css"}})))

(defn run-server
  [app port]
  (run-jetty app {:port port
                  :join? false
                  :async? true}))

(defrecord Server [server-opts database server]
  component/Lifecycle
  (start [component]
    (let [{:keys [port]} server-opts
          app* (app (:connection database))
          server* (run-server app* port)]
      (assoc component :server server*)))

  (stop [component]
    (.stop server)
    (assoc component :server nil)))

(defn create-server
  [port]
  (map->Server {:server-opts {:port port}}))
