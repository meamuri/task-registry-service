(ns com.arrival.task-registry.db
  (:require [datomic.api :as d]
            [com.stuartsierra.component :as component]))

(def database-uri "datomic:mem://task-registry")

(def task-registry-schema
  [{:db/ident :task/id
    :db/valueType :db.type/uuid
    :db/cardinality :db.cardinality/one
    :db/doc "The id of task"}

   {:db/ident :task/title
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The title of task"}

   {:db/ident :task/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The description of the task"}

   {:db/ident :task/executor
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The executor of the task"}

   {:db/ident :task/applicant
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The applicant of the task"}

   {:db/ident :task/due-date
    :db/valueType :db.type/instant
    :db/cardinality :db.cardinality/one
    :db/doc "Expected due time"}])

(defn create-conn
  ([db-uri schema]
   (d/create-database db-uri)
   (let [conn (d/connect db-uri)]
     @(d/transact conn schema)
     conn))
  ([db-uri]
   (create-conn db-uri task-registry-schema)))

(defrecord Database [schema uri connection]
  component/Lifecycle
  (start [component]
    (let [conn (create-conn uri schema)]
      (assoc component :connection conn)))

  (stop [component]
    (.release connection)
    (assoc component :connection nil)))

(defn create-database
  [uri schema]
  (map->Database {:schema schema :uri uri}))
