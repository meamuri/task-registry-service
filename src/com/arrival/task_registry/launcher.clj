(ns com.arrival.task-registry.launcher
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [com.arrival.task-registry.db :as db]
            [com.arrival.task-registry.system :as system]
            [com.arrival.task-registry.config :refer [server-port db-connection]]))

(defonce app-system nil)

(defn -main [& _args]
  (let [port (server-port)
        db-uri (db-connection)]
    (alter-var-root #'app-system (fn [_]
                                   (system/system {:db-uri db-uri
                                                   :db-schema db/task-registry-schema
                                                   :server-port port})))
    (alter-var-root #'app-system component/start-system)))
