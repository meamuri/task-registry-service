(ns com.arrival.task-registry.router
  (:require [compojure.core :refer [GET POST context routes]]
            [hiccup.page :refer [html5 include-js include-css]]
            [ring.util.response :refer [redirect]]
            [com.arrival.task-registry.domain.task.handler :as task-handler]))

(def header
  [:head
   [:title "Task Registry"]
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css "/style/sakura.css")
   (include-css "/style/layout.css")
   (include-css "/style/notification.css")])

(def main-page
  (html5
   header
   [:body
    [:div#root]
    (include-js "/js/main.js")]))

(def page-not-found
  (html5
   header
   [:body
    [:h3 "Sorry, page not found"]
    [:p "But since page hasn't content
        you can look at the pretty cat"]
    [:img {:src "/assets/cat.png"}]]))

(defn render-page
  [page]
  (fn [_request respond _raise]
    (respond {:status 200
              :headers {"Content-Type" "text/html"}
              :body page})))

(def pages
  (routes (GET "/" _ (redirect "/testapp"))
          (GET "/index.html" _ (redirect "/testapp"))
          (GET "/testapp" _ (render-page main-page))
          (GET "/tasks/:id" [_] (render-page main-page))
          (render-page page-not-found)))

(defn api-not-found
  ([request respond _]
   (respond (api-not-found request)))
  ([_request]
   {:status 404
    :body {:message "Resource not found"}}))

(defn api
  [conn]
  (routes (context "/api" []
            (context "/tasks" []
              (GET  "/" [] (task-handler/task-list-handler conn))
              (POST "/" [] (task-handler/add-task-handler conn)))
            api-not-found)))

(defn get-app [conn]
  (routes (api conn)
          pages))
