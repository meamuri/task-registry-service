(defproject com.arrival/task-registry "0.0.1"
  :description "Coffee mill core API"
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.clojure/core.async "1.3.610"]

                 ;; config
                 [aero "1.1.6"]

                 ;; system
                 [com.stuartsierra/component "1.0.0"]

                 ;; db
                 [com.datomic/datomic-pro "1.0.6269"]

                 ;; server
                 [ring/ring-core "1.9.2"]
                 [ring/ring-jetty-adapter "1.9.2"]
                 [ring/ring-json "0.5.1"]
                 [compojure "1.6.2"]
                 [hiccup "1.0.5"]

                 ;; validation
                 [metosin/malli "0.3.1"]

                 ;; format
                 [cheshire "5.10.0"]

                 ;; logging
                 [org.clojure/tools.logging "1.1.0"]
                 [org.apache.logging.log4j/log4j-api "2.14.1"]
                 [org.apache.logging.log4j/log4j-core "2.14.1"]
                 [org.apache.logging.log4j/log4j-slf4j-impl "2.14.1"]]

  :main com.arrival.task-registry.launcher
  :uberjar-name "task-registry.jar"
  :aot [com.arrival.task-registry.launcher]
  
  :jvm-opts ["-Dclojure.tools.logging.factory=clojure.tools.logging.impl/log4j-factory"]

  :profiles
  {:cljs
   {:dependencies [[org.clojure/clojurescript "1.10.764"]
                   [thheller/shadow-cljs "2.11.25"]
                   [re-frame "1.2.0"]
                   [day8.re-frame/http-fx "0.2.3"]
                   [reagent "1.0.0"]
                   [metosin/malli "0.3.1"]
                   [kibu/pushy "0.3.8"]
                   [bidi "2.1.6"]]}
    :kaocha
   {:dependencies [[lambdaisland/kaocha "1.0.829"]]}}

  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]})
